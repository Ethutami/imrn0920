console.log("");
console.log("LOOPING WHILE =================01");

//soal-1/01
console.log("");
console.log("..................LOOPING PERTAMA");
var n=2;
while(n <=20){
    console.log(n +" - I LOVE COODING");
    n+=2;
}
//soal-01/02
console.log("");
console.log("...................LOOPING KEDUA");
var m=20;
while(m >=2){
    console.log(m +" - I will become a mobile developer");
    m-=2;
}
//=========================================================================01

console.log("");
console.log("LOOPING FOR =================02");
for (var b =1; b < 21; b++){
    if (b%3 ==0 && b%2 !=0){
        console.log(b + " I Love Cooding");
    } else if (b % 2 !=0){
        console.log(b + " santai");
        }
    else { 
        console.log(b + " Berkualitas");
        }
    }
 //=========================================================================02


console.log("");
console.log("LOOPING FOR =================03");
for (var x=1; x <=4; x++){
    for(var y=1; y<=8;  y++){
        process.stdout.write("#");
    }
    console.log("");
}
//=========================================================================03


console.log("");
console.log("LOOPING FOR =================04");
for (var x=1; x <=7; x++){
    for(var y=1; y<=7 && y <x; y++){
        process.stdout.write("#");
    }
    console.log("#");
}
//=========================================================================04


console.log("");
console.log("LOOPING FOR =================05");
for (var x=1; x <=8; x++){
    for (var y=1; y<=8 ; y++){
        if((x%2!=0 && y%2!= 0) || (x%2==0 && y%2== 0)){
            process.stdout.write(" ");
        }else {
            process.stdout.write("#");
        }
    }
    console.log("");
}
//=========================================================================05
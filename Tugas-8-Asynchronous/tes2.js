// Deklarasi function yang memilik callback sebagai parameter
function periksaDokter(nomerAntri, callback) {
    if(nomerAntri > 50 ) {
        callback(true)
    } else if(nomerAntri < 10) {
        callback(false)
    }    
} 
// Menjalankan function periksaDokter yang sebelumnya sudah dideklarasi

function mycall(check){
    if(check) {
        console.log("sebentar lagi giliran saya")
    } else {
        console.log("saya jalan-jalan dulu")
    }
}

periksaDokter(65, mycall)
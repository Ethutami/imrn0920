//soal 1
console.log("");
console.log("==================================01");
let tes =[['Naya','Hern', 'female',1997],['Ahn', 'Diean','male',1996]];

function arrayToObject(){
  let now = new Date();
  let thisyear=now.getFullYear();
  for (let i = 0; i < tes.length; i++) {
    let age = tes[i][3] > thisyear ? 'Invalid Birth Year' : thisyear - tes[i][3]
    let key = `${tes[i][0]} ${tes[i][1]}`
    let obj = {
      firstName: tes[i][0],
      lastName: tes[i][1],
      gender: tes[i][2],
      age
    }
    console.log(`${key}: ${JSON.stringify(obj)}`)
  }
}
arrayToObject();



//soal 2
console.log("");
console.log("==================================02");

let tokoX =[
  {brand:"Stacattu", type:"Sepatu",           harga:1500000},
  {brand:"Zoro ",    type:"Baju",             harga:500000},
  {brand:"H&N",      type:"Baju",             harga:250000},
  {brand:"Uniklooh", type:"Sweater",          harga:175000},
  {brand:"",         type:"Casing Handphone", harga : 50000}
];

function shoppingTime(memberid, money){
  let newObject={}
  if (!memberid){
    console.log("Mohon maaf, toko X hanya berlaku untuk member saja");
  }else if (money<50000){
    console.log("Maaf, Uang tidak cukup");
  } 
  else {
    let moneyChange=money;
    let listPurchased=[];
    for(let i=0; moneyChange>=50000; i++){
     if ( moneyChange >= tokoX[i].harga){
        listPurchased.push(tokoX[i].type + " " +tokoX[i].brand)
        moneyChange -= tokoX[i].harga
     }
    }
  newObject.memberid=memberid
  newObject.money=money
  newObject.listPurchased=listPurchased
  newObject.moneyChange=moneyChange
  console.log(newObject)
  }
}
shoppingTime('hjgkguo', 2010000);




//soal 3
console.log("");
console.log("==================================03");

function naikAngkot (listpenumpang){
  let rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  let arr=[]
  
  for (let i=0; i<=listpenumpang.length-1; i++ ){
    let obj ={} //...................................................????
   
    let penumpang = listpenumpang [i][0]
    let awal      = listpenumpang [i][1]
    let akhir     = listpenumpang [i][2]

    var mulai;
    var tujuan;
    for (var j = 0; j<rute.length; j++){
        if(rute[j] == awal){
            mulai = j
        }else if (rute [j]== akhir){
            tujuan= j
        }
    }
    var bayar = (tujuan- mulai) * 2000

    obj.penumpang = penumpang
    obj.awal      = awal
    obj.akhir     = akhir
    obj.bayar     = bayar
    
    arr.push(obj)

  }
  console.log(arr)
}
naikAngkot([['den', 'C', 'D'],['hjd', 'A', 'E']])




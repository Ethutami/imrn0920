//soal 1 ------------ Arrow Function
console.log("");
console.log("==================================01");

golden = () => {
  console.log("this is golden!!")
}
golden()



//soal 2 ------------ enhanched object literals 
console.log("");
console.log("==================================02");

literal = (firstName, lastName)=> {
  return {
    firstName,
    lastName,
    fullName (){
      console.log(firstName, lastName)
    }
  }
}
literal("William", "Imoh").fullName()




//soal 3 -------------- Destructuring 
console.log("");
console.log("==================================03");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation, spell} =newObject

console.log(firstName, lastName, destination, occupation)



//soal 4 ------------- Array Spreading
console.log("");
console.log("==================================04");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = [...west, "Gill", "Brian", "Noel", "Maggie"]
console.log(east)




//soal 5 ------------- Template Literals
console.log("");
console.log("==================================05");

const planet = "earth"
const view = "glass"
var before =
 `Lorem  ${view} dolor sit amet  
consectetur adipiscing elit, 
${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam `
 
console.log(before) 

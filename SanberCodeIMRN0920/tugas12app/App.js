/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StatusBar,
} from 'react-native';

import Component from './latihan/Componet/component';
import YoutubeUI from './Tugas/Tugas12/App'
import Latihan1 from './latihan/index'
import Tesrops from './latihan/props'
import Login from './Tugas/Tugas13/LoginScreen'
import About from './Tugas/Tugas13/AboutScreen'
import EditProfile from './Tugas/Tugas13/EditScreen'
import SignUp from './Tugas/Tugas13/SignupScreen'



const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />


      {/* <YoutubeUI/> */}
      {/* <Latihan1/> */}
      {/* <Tesrops/> */}
      <Login/>
      {/* <About/> */}
      {/* <EditProfile/> */}
      {/* <SignUp/> */}

    </>
  );
};

export default App;

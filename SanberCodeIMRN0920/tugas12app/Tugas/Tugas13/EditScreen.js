import React from 'react';
import {
    View, 
    Text,
     StyleSheet,
     Image,
     TouchableOpacity,
     TextInput,
     FlatList,
     Button,
    } from 'react-native';

import Icon1 from 'react-native-vector-icons/Ionicons'
import Icon2 from 'react-native-vector-icons/AntDesign'



export default class App extends React.Component{
    // static navigationOptions = {
    //     title: "About Screen"
    // }
    // static navigationOptions = ({ navigation }) => ({
    //     title: `about with ${navigation.state.params.name}`
    // })

    render(){
        return(
            <View style={styles.container}>

                <View style={{backgroundColor :'#E0E0E0', padding:15, flexDirection :'row'}}>
                    <TouchableOpacity>
                        <Icon2 name="close" size ={25} color="#FEFEFE" />
                    </TouchableOpacity>
                    <Text style={{marginStart:25, color:'#7A5B3E', fontSize:20}}>Edit Profile</Text>
                    <TouchableOpacity>
                        <Icon2 name="check" size ={25} color="#FA4B00" style={{marginStart:170}} />
                    </TouchableOpacity>
                </View>
               
                <View style={styles.user}>
                    <Image source={require('./Images/photo.png')} style={{width:100, height: 100, justifyContent:'center'}} />
                    <TouchableOpacity >
                        <Text style={{paddingTop:13, paddingBottom:8, color:'#13747D'}}>Change Profile Photo</Text>
                    </TouchableOpacity>
                </View>

                <View style={{flexDirection:'column'}}>
                   
                    <TextInput 
                    style={styles.input}
                    placeholder="Username"/>

                    <TextInput 
                    style={styles.input}
                    placeholder="Bio"/>
                   
                   
                    <View style={styles.add} >
                        <View style={styles.icons} >
                            <TouchableOpacity>
                            <Icon1 name="add" size={25} color='#CDBDAE'/>
                            </TouchableOpacity>
                            <TouchableOpacity>
                            <Icon1  name="add" size={25} color='#CDBDAE' />
                            </TouchableOpacity> 
                        </View>

                        <View style={{padding:5}}></View>

                        <View>
                                <Text style={{color :'#CDBDAE', fontSize: 15, paddingTop:10}}>Portfolio</Text>
                                <Text style={{color :'#CDBDAE', fontSize: 15, paddingTop:10}}>Contact</Text>

                        </View>
                    </View>
                    <Text style={styles.lines }></Text>

                </View>
                
               
               
                    
                    
          
                
                
                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor :'#FEFEFE'  
      },
    user :{
        padding : 40,
        
        flexDirection : 'column',
        alignItems : 'center',
        paddingBottom:40
        
    },
   

    input :{
        marginVertical:8,
        marginHorizontal :15,
        justifyContent:'flex-start', 
        fontSize :15,
        borderBottomWidth:1,
        marginHorizontal :15,
        borderBottomColor :'#CDBDAE'


    },
    add :{
        marginHorizontal:15,
        flexDirection: 'row'
        
    },
    
    icons :{
        flexDirection :'column',
        padding:4,
        justifyContent : 'center',
        color : '#CDBDAE'
        
        
    },
    lines :{
        borderBottomWidth:1,
        marginHorizontal :15,
        borderBottomColor :'#CDBDAE'
    }

    })
import React from 'react';
import {
    View, 
    Text,
     StyleSheet,
     Image,
     TouchableOpacity,
     FlatList,
     Button,
    } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'
import Icon1 from 'react-native-vector-icons/AntDesign'

export default class App extends React.Component{
    // static navigationOptions = {
    //     title: "About Screen"
    // }
    // static navigationOptions = ({ navigation }) => ({
    //     title: `about with ${navigation.state.params.name}`
    // })

    render(){
        return(
            <View style={styles.container}>
                <Text style={{textAlign:'center', marginTop : 35, fontSize:45, color :'#1F1F1F', fontWeight :'bold'}}>About Me</Text>
                
                <View style={styles.user}>
                    <Image source={require('./Images/Ellipse1.png')} style={{width:100, height: 100, justifyContent:'center'}} />
                    <TouchableOpacity >
                        <Text style={{paddingTop:13, paddingBottom:8, color:'#13747D'}}>Edit Profile</Text>
                    </TouchableOpacity>
                    <Text style={{color :'#1F1F1F', fontSize :24, fontWeight:'bold'}}>Ethika Utami Sugiati</Text>
                    <Text style={{color :'#FA4B00', fontSize :20, }}>React Native Developer</Text>
                </View>
                
                <View style={{backgroundColor :'#E0E0E0', marginHorizontal:30, borderRadius:3, padding:10}}>
                    <Text style={styles.portflio}>Portfolio</Text>
                    <View style={{flexDirection:'row', justifyContent:'center', padding:8}}>
                        <TouchableOpacity>
                            <Icon1 style={{paddingStart:20}} name="github" size={25} color='#FA4B00'/>
                            <Text style={{color :'#FA4B00', fontSize:15}}>@Ethutami</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{paddingLeft:50}} >
                        <Icon1 style={{paddingStart:20}} name="gitlab" size={25} color='#FA4B00'/>
                            <Text style={{color :'#FA4B00', fontSize:15}}>@Ethutami</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{backgroundColor :'#E0E0E0', marginHorizontal:30, borderRadius:3, padding:10, marginTop:10}}>
                    <Text style={styles.portflio}>Contact Me</Text>
                    <View style={{flexDirection :'row', justifyContent:'center'}}>
                        <View style={styles.icons} >
                            <Icon1  name="instagram" size={25} color='#FA4B00'/>
                            <Icon1 style={{paddingTop:8}} name="twitter" size={25} color='#FA4B00'/>
                            <Icon1 style={{paddingTop:8}} name="facebook-square" size={25} color='#FA4B00'/>
                        </View>

                        <View style={{padding:10}}></View>

                        <View style={styles.icons}>
                            <TouchableOpacity>
                                <Text style={{color :'#FA4B00', fontSize:15, }}>@Ethutami</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style={{color :'#FA4B00', fontSize:15, paddingTop :15}}>@Ethutami</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Text style={{color :'#FA4B00', fontSize:15, paddingTop :15}}>Thika Fahrezy</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    
                </View>
                
                
                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor :'#FEFEFE'  
      },
    user :{
        padding : 10,
        flexDirection : 'column',
        alignItems : 'center',
        paddingBottom:40
        
    },
   

    portflio :{
        paddingLeft :10,
        justifyContent:'flex-start', 
        color:'#CDBDAE',
        borderBottomColor :'#CDBDAE', 
        borderBottomWidth :1

    },
    
    icons :{
        flexDirection :'column',
        padding:8,
        justifyContent : 'center',
        
    },
    })
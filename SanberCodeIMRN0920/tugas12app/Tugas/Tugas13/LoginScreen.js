import React from 'react';
import {
    View, 
    Text,
     StyleSheet,
     Image,
     TouchableOpacity,
     TextInput

    } from 'react-native';


//import routes from './Router';
// export default StackNavigator(routes);
//import { StackNavigator } from 'react-navigation';

// import About from './AboutScreen'
// import SignUp from './SignupScreen'

// const routes = {
//     about: { screen: About },
//     signup: { screen: SignUp }
// }
// export default routes;

export default class App extends React.Component{
    // _onPressButton(){

    //     <About/>
    
    //   };
    constructor(props) {

        super(props);
    
        this.state = {text: ''};
    
      }

    render(){
        
        return(
            <View style={styles.container}>
                <View style={styles.logo} >
                    <Image source={require('./Images/logo.png')} style={{width:240, height: 55}} />
                </View>
                <View style={{ marginHorizontal:10, marginTop:36}}>
                    <Text style={{fontSize: 30, paddingBottom:30,textAlign :'center', color:'#7A5B3E'}} >Log In</Text>
                </View>
                <View style={{flexDirection :'column'}}>
                <View style={styles.inputname}>
                    <TextInput 
                    style={{paddingLeft :10,paddingVertical:10, justifyContent:'flex-start', color:'#1F1F1F', }} 
                    placeholder="Username"
                    ></TextInput>
                </View>
                <View style={styles.inputpass}>
                    <TextInput 
                    style={{paddingLeft :10,paddingVertical:10, justifyContent:'flex-start', color:'#1F1F1F', }} 
                    placeholder="Password"
                    ></TextInput>
                </View>

                    <TouchableOpacity 
                    style={styles.SubmitButtonStyle} 
                    activeOpacity = { 0.5 }
                    //onPress={() => this.props.navigation.navigate('about', { name: 'Daengweb.id' })}
                    >
                        <Text style={styles.TextStyle}> Log In </Text>
                    </TouchableOpacity>

                   

                    <View style={{paddingTop :30, flexDirection:'row', justifyContent :'center'}}>
                        <Text >No account ?</Text>
                        <Text >    </Text>
                        <TouchableOpacity activeOpacity = { 0.5 }>
                        <Text style={{color:'#13747D'}}>Sign up!</Text>
                        </TouchableOpacity>

                    </View>

                </View>
              

            </View>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor :'#FEFEFE'  
      },
    logo :{
        padding : 18,
        flexDirection : 'column',
        alignItems : 'center'
    },
    SubmitButtonStyle: {
        marginTop:30,
        paddingVertical:10,
        marginHorizontal :15,
        backgroundColor:'#FA4B00',
        borderRadius:8,
      },
      TextStyle:{
          color:'#FAFAFA',
          textAlign:'center',
          fontSize :20,
          fontFamily :'AnticSlab-Regular'
          
      },
      inputname :{
        backgroundColor :'#E0E0E0', 
        marginHorizontal:15, 
        borderRadius:3
      },
      inputpass :{
        marginTop :10,
        backgroundColor :'#E0E0E0', 
        marginHorizontal:15, 
        borderRadius:3
      }



})